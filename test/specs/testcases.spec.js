var assert = require('assert');
var actions = require('./actions.js');
var Mockaroo = require('mockaroo');


describe('e2e testing -> losestudiantes.co', function() {

    var client = new Mockaroo.Client({
        apiKey: 'xxx'// your api key here
    });

    it('SignUp', function() {
        client.generate({
            count: 1,
            fields: [{
                name: 'first_name',
                type: 'First Name'
            }, {
                name: 'last_name',
                type: 'Last Name'
            }, {
                name: 'email',
                type: 'Email Address'
            }, {
                name: 'password',
                type: 'Password'
            }, {
                name: 'program',
                type: 'Number',
                min: 1,
                max: 30,
                decimals: 0
            }, {
                name: 'check',
                type: 'Boolean'
            }]
        }).then(function(record) {
            actions.gotoHomepage(true);
            actions.signup(record.first_name, record.last_name, record.email, record.password, record.program, record.check, 'Ocurrió un error activando tu cuenta');
        });
        
    });

    it('LogIn / LogOut Success', function () {
        
        client.generate({
            count: 1,
            fields: [{
                name: 'email',
                type: 'Custom List',
                values: ['f.arruza@uniandes.edu.co','fabiantriana1072@gmail.com']
            }, {
                name: 'password',
                type: 'Custom List',
                values: ['xxxxxx','xxxxx']
            }]
        }).then(function(record) {
            actions.gotoHomepage();
            actions.login(record.email, record.password, '');
            actions.logout();
        });
        
    });

    it('LogIn Failed', function () {
        client.generate({
            count: 1,
            fields: [{
                name: 'email',
                type: 'Email Address'
            }, {
                name: 'password',
                type: 'Password'
            }]
        }).then(function(record) {
            actions.gotoHomepage();
            actions.login(record.email, record.password, 'Intenta de nuevo por favor.');
        });
    });

    it('Search professor', function() {
        
        client.generate({
            count: 1,
            fields: [{
                name: 'profesor',
                type: 'Custom List',
                values: ['Fernando Arruza Hedman', 'Mario Linares Vasquez']
            }]
        }).then(function(record) {
            actions.gotoHomepage();
            browser.waitForVisible('input[role="combobox"]', 5000);
            browser.setValue('input[role="combobox"]', record.profesor);
            actions.delay(2000);
            browser.element('input[role="combobox"]').keys("Enter");

            browser.waitForVisible('.descripcionProfesor', 5000);
            expect(browser.getText('.nombreProfesor')).to.include(record.profesor);
        });
        
        
    });
});
